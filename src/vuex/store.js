import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    products: [],
    cart: [],
  },
  mutations: {
    SET_PRODUCTS_TO_STATE: (state, products) => {
      state.products = products;
    },
    SET_CART: (state, product) => {
      const existProduct = state.cart.find(
        (item) => item.article === product.article
      );
      existProduct ? existProduct.quantity++ : state.cart.push(product);
    },
    REMOVE_FROM_CART: (state, indexCartItem) => {
      state.cart.splice(indexCartItem, 1);
    },
    INCREMENT: (state, id) => {
      const item = state.cart.find((item) => item.id === id);
      item && item.quantity++;
    },
    DECREMENT: (state, id) => {
      const item = state.cart.find((item) => item.id === id);
      item && item.quantity--;
    },
  },
  actions: {
    async GET_PRODUCTS({ commit }) {
      try {
        const products = await axios.get("http://localhost:3000/products");
        commit("SET_PRODUCTS_TO_STATE", products.data);
        return products;
      } catch (err) {
        console.log(err);
        return err;
      }
    },
    ADD_TO_CART({ commit }, product) {
      commit("SET_CART", product);
    },
    DELETE_FROM_CART({ commit }, index) {
      commit("REMOVE_FROM_CART", index);
    },
    INCREMENT_ITEM_QUANTITY({ commit }, id) {
      commit("INCREMENT", id);
    },
    DECREMENT_ITEM_QUANTITY({ commit }, id) {
      commit("DECREMENT", id);
    },
  },
  getters: {
    PRODUCTS(state) {
      return state.products;
    },
    CART(state) {
      return state.cart;
    },
  },
});

export default store;
